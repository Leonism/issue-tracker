<!-- TODO: add active class to the current page -->
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon">
            <i class="fas fa-list-check"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Issue Tracker</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-house"></i>
            <span>Home</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Lists
    </div>

    <!-- Nav Item - Issues -->
    <li class="nav-item">
        <a class="nav-link" href="issues.php">
        <i class="fa-solid fa-table-list"></i>
            <span>Issues</span></a>
    </li>

    <!-- Nav Item - Users -->
    <li class="nav-item">
        <a class="nav-link" href="Users.php">
            <i class="fa-solid fa-user-group"></i>
            <span>Users</span></a>
    </li>

    <!-- Nav Item - Departments -->
    <li class="nav-item">
        <a class="nav-link" href="departments.php">
            <i class="fa-solid fa-people-group"></i>
            <span>Departments</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->