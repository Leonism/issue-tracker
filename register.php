<?php
include_once 'includes/db/departments/functions.inc.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Issue Tracker</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <form class="user" method="POST" action="includes/db/users/insert.inc.php">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="name" placeholder="Full Name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Email Address" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" placeholder="Username" />
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control" name="pwd" placeholder="Password" />
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control" name="pwdrepeated" placeholder="Repeat Password" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="department">
                                        <option value="" selected>No Depatment</option>
                                        <?php showDepartmentsOptions(); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="ml-1">
                                        <label class="">Admin
                                        </label>
                                        <input type="hidden" name="admin" value="0" checked />
                                        <input type="checkbox" class="ml-1" name="admin" value="1" />
                                    </div>
                                </div>

                                <?php
                                if (isset($_GET["error"])) {

                                    echo "<div class=\"text-danger font-weight-bold text-center form-group\">";

                                    if ($_GET["error"] == "infomissing")
                                        echo "Some info is missing!";

                                    else if ($_GET["error"] == "invalidusername")
                                        echo "Invalid username. Use only lower, uppercase letters and numbers!";

                                    else if ($_GET["error"] == "invalidEmail")
                                        echo "Invalid Email!";

                                    else if ($_GET["error"] == "pwdnotmatched")
                                        echo "Password and repeated password don't match!";

                                    else if ($_GET["error"] == "usernametaken")
                                        echo "Username is already taken. Please choose a different one!";

                                    echo "</div>";
                                }
                                ?>

                                <input class="btn btn-primary btn-block" name="submitted" type="submit" value="Register Account" />
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/script.js"></script>
</body>

</html>