<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - User Information -->
        <?php
        if (isset($_SESSION["userName"])) {
            echo "<li class=\"nav-item dropdown no-arrow\">
            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                <span class=\"mr-2 d-none d-lg-inline text-gray-600 font-weight-bold\">" . $_SESSION["userName"] . "</span>
                <img class=\"img-profile rounded-circle\" src=\"img/undraw_profile.svg\">
            </a>
            <!-- Dropdown - User Information -->
            <div class=\"dropdown-menu dropdown-menu-right shadow animated--grow-in\">
                <a class=\"dropdown-item\" href=\"profile.php\">
                    <i class=\"fas fa-user fa-sm fa-fw mr-2 text-gray-400\"></i>
                    Profile
                </a>
                <div class=\"dropdown-divider\"></div>
                <a href=\"includes/db/users/logout.inc.php\" class=\"dropdown-item\">
                    <i class=\"fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400\"></i>
                    Logout
                </a>
            </div>
        </li>";
        } else {
            echo "<a class=\"mr-2 d-none d-lg-inline font-weight-bold text-gray-600\" href=\"login.php\">Login</a>";
        }
        ?>
    </ul>

</nav>