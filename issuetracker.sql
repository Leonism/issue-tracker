-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2022 at 12:07 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `issuetracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(2, 'Finance'),
(3, 'Hotline'),
(1, 'IT');

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE `issues` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `body` varchar(10000) NOT NULL,
  `creator` varchar(50) NOT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `stage` enum('Open','In Progress','Resolved','Closed') DEFAULT 'Open',
  `department` varchar(20) DEFAULT NULL,
  `priority` enum('Low','Normal','High') NOT NULL DEFAULT 'Normal',
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `issues`
--

INSERT INTO `issues` (`id`, `title`, `body`, `creator`, `owner`, `stage`, `department`, `priority`, `created`) VALUES
(6, 'Prepared statement on submitting an issue', 'Use prepared statements for improved security and reliability when inserting a new issue', '9', '9', 'Closed', '1', 'High', '2022-03-21 16:03:41'),
(7, 'Updating README', 'Some information is missing from the readme. Documentation should be completed.', '10', '', 'Open', '3', 'Normal', '2022-03-22 10:03:37'),
(8, 'Revoking admin privileges', 'Admins can remove their own admin privileges on the users page. Does it make sense having this feature? If yes we should make it possible on the profile page.', '10', '9', 'In Progress', '1', 'Low', '2022-03-22 10:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `issueID` int(6) UNSIGNED NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `body` varchar(10000) NOT NULL,
  `creator` int(10) UNSIGNED NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`issueID`, `title`, `body`, `creator`, `created`) VALUES
(6, 'Script', 'Updated the insert function to use a prepared statement.', 9, '2022-03-22 06:03:38'),
(6, 'Testing complete', 'The test of the new insert script has been successful. 0 Errors.', 9, '2022-03-22 10:03:33'),
(8, 'Changed to low priority', 'You can revoke your own admin privileges on the user page. The admin will still have admin privileges until they log out and log in to reload session variables, so if they made a mistake by revoking their own admin privileges they can just add them to themselves again.', 9, '2022-04-05 12:04:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0,
  `department` varchar(20) DEFAULT NULL,
  `pwd` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `admin`, `department`, `pwd`) VALUES
(9, 'Arthur Morgan', 'Arthur.Morgan@somecompany.com', 'arthur', 1, '1', '$2y$10$6BiOtUfisWuWP7mub5P4NO/bTm7FJnSbssUp3SETiT8To2dubnHCq'),
(10, 'Randy Savage', 'macho.nacho@dip.com', 'themachoman', 0, '3', '$2y$10$j2Wp91DT5oPQ3It97EsfyuBiFX0iUFAn7Hf0n5J3pmyoRbNk4BUEe'),
(11, 'Bruce Willis', 'willi@notthewhale.com', 'imbatman', 1, '2', '$2y$10$nyTqTYscm0A/bjwE0UvLwedBxW9PrPvMP2IeEmGGxQi0CVqkjq2f2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_name` (`name`);

--
-- Indexes for table `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `issues`
--
ALTER TABLE `issues`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
