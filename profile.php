<?php
session_start();
if (!isset($_SESSION["userName"])) {
    header("location: login.php");
}
include_once 'header.php';
include_once 'includes/db/departments/functions.inc.php';
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Profile</h1>
</div>
<div class="">
    <!-- TODO: Search for admins in the same department option. Clicking on department takes the user to the admin list -->
    <!-- TODO: Update successful message -->
    <!-- TODO: Add the ability to change admin status -->
    <!-- Profile Card -->
    <div class="">
        <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ml-1">
                    Name</div>
                <div class="row no-gutters align-items-center mb-3">
                    <div class="col mr-2">
                        <form method="POST" action="includes/db/users/profileupdate.inc.php">
                            <?php
                            echo "<input type=\"text\" class=\"form-control-profile\" name=\"name\" value=\"" . $_SESSION["userName"] . "\"/>";
                            ?>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                </div>
                <div class="row no-gutters align-items-center mb-3">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Username</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php
                            echo $_SESSION["userUsername"];
                            ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-id-card fa-2x text-gray-300"></i>
                    </div>
                </div>
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ml-1">
                    E-Mail</div>
                <div class="row no-gutters align-items-center mb-3">
                    <div class="col mr-2">
                        <?php
                        echo "<input type=\"text\" class=\"form-control-profile\" name=\"email\" value=\"" . $_SESSION["userEMail"] . "\"/>";
                        ?>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-at fa-2x text-gray-300"></i>
                    </div>
                </div>
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ml-1">
                    Department</div>
                <div class="row no-gutters align-items-center mb-3">
                    <div class="col mr-2">
                        <select class="form-control-profile" name="department">
                            <option value="">No Depatment</option>
                            <?php showDepartmentsOptions(); ?>
                        </select>
                    </div>
                    <div class="col-auto">
                        <i class="fa-solid fa-people-group fa-2x text-gray-300"></i>
                    </div>
                </div>
                <div class="row no-gutters align-items-center mb-3">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Administrator</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php
                            if ($_SESSION["userAdmin"] == 1)
                                echo "<i class=\"fa-solid fa-check\"></i>";
                            else
                                echo "<i class=\"fa-solid fa-xmark\"></i>";
                            ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user-gear fa-2x text-gray-300"></i>
                    </div>
                </div>
                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block" />
                <div class="d-flex justify-content-end position-relative">
                    <input class="btn btn-primary" name="submitted" type="submit" value="Save Changes"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once('footer.php');
?>