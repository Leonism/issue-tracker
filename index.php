<?php
include_once 'header.php';
?>
<div class="row">
    <div class="col-lg-6 mb-4">

        <!-- Issues -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa-solid fa-table-list"></i> Issues</h6>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="height: 7rem;" src="img/undraw_collaboration_re_vyau.svg" alt="...">
                </div>
                <p>Track issues, bugs or tickets. Doesn't matter what you call them for what purpose. Submit tickets, track their progress and current state!</p>
                <a target="_blank" href="issues.php">Browse issues &rarr;</a>
            </div>
        </div>

        <!-- Profile -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-user"></i> Profile</h6>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="height: 7rem;" src="img/undraw_wall_post_re_y78d.svg" alt="...">
                </div>
                <p>Once you are registered by an administrator you can submit issues and attach notes to them.</p>
                <a target="_blank" href="profile.php">Go to your profile & edit your info &rarr;</a>
            </div>
        </div>
    </div>
    <div class="col-lg-6 mb-4">

        <!-- Users -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa-solid fa-user-group"></i> Users</h6>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="height: 7rem;" src="img/undraw_connecting_teams_re_hno7.svg" alt="...">
                </div>
                <p>Check which users work in which department and if they have administartor previliges to find out who can help with a certain issue.</p>
                <a target="_blank" href="users.php">Find a suitable contact person here &rarr;</a>
            </div>
        </div>

        <!-- Departments -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-user"></i> Departments</h6>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="height: 7rem;" src="img/undraw_connected_re_lmq2.svg" alt="...">
                </div>
                <p>Administrators can create departments so you can assign issues to certain ones that meet their expertise.</p>
                <a target="_blank" href="departments.php">Already existing departments &rarr;</a>
            </div>
        </div>

    </div>
</div>
<!-- Issue's card -->
<?php
include_once('footer.php');
?>