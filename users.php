<?php
include_once 'header.php';
include_once 'includes/db/users/show.inc.php';
?>
<!-- TODO: Handle user deletion. Those can't be shown as assignees nor creators of issues or notes.-->
<!-- Table's card -->
<div class="card shadow mb-4 mt-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Users</h6>
        <div class="d-flex align-items-center">
            <?php
            if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                echo "<a href=\"register.php\" class=\"btn btn-info btn-circle btn-sm mr-2\">
                    <i class=\"fa-solid fa-plus\"></i>
                    </a>";
            }
            ?>

            <!-- Table Search -->
            <form>
                <input type="text" id="searchInput" class="form-control bg-light small" placeholder="Search for..." onkeyup="searchTable()">
            </form>
        </div>

    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div class="text-success font-weight-bold">
                <?php
                if (isset($_GET['add'])) {
                    if ($_GET['add'] == "success") {
                        echo "Successfully added!";
                    }
                } else if (isset($_GET['delete'])) {
                    if ($_GET['delete'] == "success") {
                        echo "Successfully deleted!";
                    }
                }
                ?>
            </div>
            <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>E-Mail</th>
                        <th>Department</th>
                        <th>Admin</th>
                        <?php
                        if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                            echo "<th>Actions</th>";
                        }
                        ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>E-Mail</th>
                        <th>Department</th>
                        <th>Admin</th>
                        <?php
                        if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                            echo "<th>Actions</th>";
                        }
                        ?>
                    </tr>
                </tfoot>
                <tbody>
                    <?php ShowData(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- footer -->
<?php
include_once('footer.php');
?>

<script src="js/users.js"></script>