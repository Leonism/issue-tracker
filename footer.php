</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Bottombar -->
<?php
include_once('bottombar.php');
?>
<!-- End of Bottombar -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/script.js"></script>

</body>

</html>