<?php
include_once 'header.php';
include_once 'includes/db/departments/show.inc.php';
?>
<!-- Table's card -->
<div class="card shadow mb-4 mt-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Departments</h6>
        <div class="d-flex align-items-center">
            <?php
            if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                echo "<a id=\"add-pc-btn\" class=\"btn btn-info btn-circle btn-sm mr-2\">
                <i class=\"fa-solid fa-plus\"></i>
                </a>";
            }
            ?>

            <!-- Table Search -->
            <form>
                <input type="text" id="searchInput" class="form-control bg-light small" placeholder="Search for..." onkeyup="searchTable()">
            </form>
        </div>

    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div class="text-success font-weight-bold">
                <?php
                if (isset($_GET['add'])) {
                    if ($_GET['add'] == "success") {
                        echo "Successfully added!";
                    }
                } else if (isset($_GET['delete'])) {
                    if ($_GET['delete'] == "success") {
                        echo "Successfully deleted!";
                    }
                }
                ?>
            </div>
            <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <?php
                        if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                            echo "<th>Actions</th>";
                        }
                        ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <?php
                        if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                            echo "<th>Actions</th>";
                        }
                        ?>
                    </tr>
                </tfoot>
                <tbody>
                    <?php ShowData(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- footer -->
<?php
include_once('footer.php');
?>

<!-- popup Modal-->
<div class="add-popup">
    <div class="container-fluid add-popup-content">
        <div class="card shadow">
            <div class="card-header py-3 d-flex justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Add a new Department</h6>
                <a href="#" id="close-popup-btn" class="btn btn-danger btn-circle btn-sm">
                    <i class="fa-solid fa-close"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="add-pc-form">
                    <form method="POST" action="includes/db/departments/insert.inc.php">
                        <div class="mb-3">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Title" />
                        </div>
                        <input class="btn btn-primary" name="submitted" type="submit" value="Submit" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/departments.js"></script>
<script src="js/pop-up.js"></script>