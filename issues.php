<?php
include_once 'header.php';
include_once 'includes/db/issues/show.inc.php';
include_once 'includes/db/departments/functions.inc.php';
include_once 'includes/db/users/functions.inc.php';
?>
<!-- Table's card -->
<div class="card shadow mb-4 mt-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Issues</h6>
        <div class="d-flex align-items-center">
            <?php
            if (isset($_SESSION["userID"])) {
                echo "<a id=\"add-pc-btn\" class=\"btn btn-info btn-circle btn-sm mr-2\">
                <i class=\"fa-solid fa-plus\"></i>
                </a>";
            } else {
                echo "Login to submit an issue!";
            }
            ?>
            <!-- Table Search -->
            <form>
                <input type="text" id="searchInput" class="form-control bg-light small" placeholder="Search for..." onkeyup="searchTable()">
            </form>
        </div>

    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div class="text-success font-weight-bold">
                <?php
                if (isset($_GET['add'])) {
                    if ($_GET['add'] == "success") {
                        echo "Successfully added!";
                    }
                } else if (isset($_GET['delete'])) {
                    if ($_GET['delete'] == "success") {
                        echo "Successfully deleted!";
                    }
                }
                ?>
            </div>
            <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>By</th>
                        <th>Assignee</th>
                        <th>Stage</th>
                        <th>Department</th>
                        <th>Priority</th>
                        <th>Date Created</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>By</th>
                        <th>Assignee</th>
                        <th>Stage</th>
                        <th>Department</th>
                        <th>Priority</th>
                        <th>Date Created</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php showIssues(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- footer -->
<?php
include_once('footer.php');
?>

<!-- popup Modal-->
<div class="add-popup">
    <div class="container-fluid add-popup-content">
        <div class="card shadow">
            <div class="card-header py-3 d-flex justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Submit an Issue</h6>
                <a href="#" id="close-popup-btn" class="btn btn-danger btn-circle btn-sm">
                    <i class="fa-solid fa-close"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="add-pc-form">
                    <form method="POST" action="includes/db/issues/insert.inc.php">
                        <div class="mb-3">
                            <label class="form-label">Title</label>
                            <input type="text" class="form-control" name="title" placeholder="Title" />
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Body</label>
                            <textarea class="form-control" name="body" rows="2" cols="100" placeholder="Your message..."></textarea>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Assignee (Optional)</label>
                            <select class="form-control mdb-select md-form" name="owner">
                                <option value="" selected>No Assignee</option>
                                <?php ShowUsersOptions(); ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label ">Depatment</label>
                            <select class="form-control" name="department">
                                <option value="" selected>No Depatment</option>
                                <?php showDepartmentsOptions(); ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label ">Priority</label>
                            <select class="form-control" name="priority" default="Normal">
                                <option value="Low">Low</option>
                                <option value="Normal" selected="selected">Normal</option>
                                <option value="High">High</option>
                            </select>
                        </div>
                        <?php echo "<input type=\"text\" style=\"display: none;\" name=\"creator\" value=\"" . $_SESSION["userID"] . "\"/>";
                        ?>
                        <input class="btn btn-primary" name="submitted" type="submit" value="Submit" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/pop-up.js"></script>
<script src="js/issues.js"></script>