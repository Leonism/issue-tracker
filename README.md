# Issue Tracker

This project is a practice project focused on PHP, HTML, JavaScript, MySQL and CSS (Bootstrap 4).
Issue Tracker is a multi-purpose portal, where administrators and users can track and report issues, bugs and tickets.

## Table of contents

* [Technologies](#technologies)
* [Features](#features)
* [ER Model](#er-model)
* [Database Schema](#database-schema)
* [Walkthrough and Screenshots](#walkthrough-and-screenshots)
* [Download and Usage](#download-and-usage)
* [Bugs and Issues](#bugs-and-issues)
* [Theme](#theme)
* [License](#license)


## Technologies

* Server-side computations and functions in PHP. E.g: Logging in system, Database connection and operations, error handling.
* MySQL.
* CSS, responsive Bootstrap 4 theme.
* HTML.
* Dynamic pages using JavaScript.

## Features

* Submitting issues and attaching notes to them.
* Registering and logging in system.
* Administrators have higher previliges than other users. E.g.: Adding departments and renaming them.
* Each logged in user can edit their profile and contact info.
* Add departments and assign issues to them.

## ER Model

![ER Model](/source/images/er-model.png "ER Model")*ER Model of the relational database*

## Database Schema

![DB Schema](/source/images/db-schema.png "DB Schema")*Database Schema*

## Walkthrough and Screenshots

Home view: an overview of the features.<br><br>
<img src="/source/images/index.png"  width="640" height="360"><br><br>

Issues' view: a list of all submitted issues.<br><br>
![Table of all submitted issues](/source/images/issues.png "Issues' list")<br><br><br>

After openning any issue, logged in users can attach notes to them and assign a user to the issue. They can also change the stage, the priority and the department the issue is assigned to.<br><br>
![Notes attached to an issue](/source/images/notes.png "Notes")<br><br><br>

We can best see the dynamic aspect of the wep-application using JavaScript on the Departments' list page.<br><br>
![Table of all departments](/source/images/departments.png "departments' list")<br><br><br>

The buttons on the right in each row offer editing, duplicating and deletion options.<br><br>
![Available actions](/source/images/actions.png "actions")<br><br><br>

Clicking the edit button will dynamically create an input field that will replace the content of the cell, hide the edit and duplicate buttons and show the cancel and the save/update buttons.
The update button invokes a method that dynamically creates a form and submits it with the new entered information.
The cancel button restores the content of the cell.<br><br>
![After clicking the edit button](/source/images/edit.png "editing")<br><br><br>

Register form.<br><br>
![Adding a new user account](/source/images/register.png "Register form")<br><br><br>

Log in form.<br><br>
![Logging in as an existing user](/source/images/login.png "Login")<br><br><br>

Handling errors and feedback messages, which are handled on the server side using php.<br><br>
![Register form error](/source/images/error1.png "Invalid username")<br>
![Insertion in database successful](/source/images/message1.png "Submitting successful")<br><br><br>

Profile view.<br><br>
![Editing contact info](/source/images/profile.png "Profile")<br><br><br>

Users' view.<br><br>
![Table of all users](/source/images/users.png "Users")<br><br><br>

## Download and Usage

1. Set up a local server that can run PHP and any MySQL database.
2. Create a database called "issuetracker".
3. Import issuetracker.sql to your database.
4. Copy the repository to a directory, where you can call and open the site from your browser (localhost).

## Bugs and Issues

You can use this project for any purpose at your own risk. The code is delivered as is and I'm not responsible for any issues, bugs or problems that might be included or emerge in the future.

## Theme

The main theme of this project was made using the free template [SB Admin 2](https://startbootstrap.github.io/startbootstrap-sb-admin-2)
released under the [MIT](https://github.com/StartBootstrap/startbootstrap-resume/blob/master/LICENSE) license.

## License

The project has been released under the [MIT license](/LICENSE). You can use this project for any purpose, even for commercial projects.
