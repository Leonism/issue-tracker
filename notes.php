<?php
include_once 'header.php';
include_once 'includes/db/notes/show.inc.php';
?>
<!-- Issue's card -->
<?php
if (isset($_GET['issue']))
    showIssueNote($_GET['issue']);
else
    echo "INVALID ISSUE URL!";

include_once('footer.php');
?>

<!-- popup Modal-->
<div class="add-popup">
    <div class="container-fluid add-popup-content">
        <div class="card shadow">
            <div class="card-header py-3 d-flex justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Submit a Note</h6>
                <a href="#" id="close-popup-btn" class="btn btn-danger btn-circle btn-sm">
                    <i class="fa-solid fa-close"></i>
                </a>
            </div>
            <div class="card-body">

                <form method="POST" action="includes/db/notes/insert.inc.php">
                    <div class="mb-3">
                        <label class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Title" />
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Body</label>
                        <textarea class="form-control" name="body" rows="2" cols="100" placeholder="Your message..."></textarea>
                    </div>
                    <?php echo "<input type=\"text\" style=\"display: none;\" name=\"creator\" value=\"" . $_SESSION["userID"] . "\"/>
                                    <input type=\"text\" style=\"display: none;\" name=\"issueID\" value=\"" . $_GET['issue'] . "\"/>";
                    ?>
                    <input class="btn btn-primary" name="submitted" type="submit" value="Submit" />
                </form>
            </div>
        </div>
    </div>
</div>

<script src="js/pop-up.js"></script>
<script src="js/notes.js"></script>