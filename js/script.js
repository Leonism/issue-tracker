function EditRow(element) {

  // Get row
  let row = element.closest("tr");
  row.setAttribute("Editing", "true");

  const cells = row.getElementsByClassName("info-editable");
  const editBtn = row.getElementsByClassName("btn-edit");
  const updateBtn = row.getElementsByClassName("btn-update");
  const cancelBtn = row.getElementsByClassName("btn-cancel");
  const duplicateBtn = row.getElementsByClassName("btn-duplicate");

  // Show/hide buttons
  editBtn[0].style.display = "none";
  updateBtn[0].style.display = "";
  cancelBtn[0].style.display = "";
  duplicateBtn[0].style.display = "none";

  // Add the value to an input field and append it
  for (let i = 0; i < cells.length; i++) {
    cells[i].innerHTML = "";
    const input = document.createElement("input");
    input.classList.add("form-control");
    input.type = "text";
    input.name = cells[i].getAttribute("name");
    input.value = cells[i].getAttribute("info");
    cells[i].appendChild(input);
  }

}

function CancelEdit(element) {

  let row = element.closest("tr");
  if (row.getAttribute("editing") == "true") {
    row.setAttribute("Editing", "false");

    const cells = row.getElementsByClassName("info-editable");
    const editBtn = row.getElementsByClassName("btn-edit");
    const updateBtn = row.getElementsByClassName("btn-update");
    const cancelBtn = row.getElementsByClassName("btn-cancel");
    const duplicateBtn = row.getElementsByClassName("btn-duplicate");

    // Show/hide buttons    
    editBtn[0].style.display = "";
    updateBtn[0].style.display = "none";
    cancelBtn[0].style.display = "none";
    duplicateBtn[0].style.display = "";

    // Make the value inside the field the original value
    for (let i = 0; i < cells.length; i++) {
      cells[i].innerHTML = cells[i].getAttribute("info");
    }
  }
}

function searchTable() {
  // Declare variables
  var input, filter, table, tr, td, txtValue;
  input = document.getElementById("searchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("dataTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 2; i < tr.length; i++) {
    td = tr[i];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

(function ($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function () {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };

    // Toggle the side navigation when window is resized below 480px
    if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
      $("body").addClass("sidebar-toggled");
      $(".sidebar").addClass("toggled");
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function (e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

})(jQuery); // End of use strict
