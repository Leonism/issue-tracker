function UpdateRow(element) {

  const row = element.closest("tr");
  const newInfo = row.getElementsByTagName("input");
  const ids = row.getElementsByClassName("info-uneditable");

  const oldname = row.getElementsByClassName("info-editable")[0].getAttribute("info");

  if (confirm("Update " + oldname + "?") == true) {

    // Dynamically create <input>s in a form and submit it

    const form = document.createElement('form');
    form.method = "post";
    form.action = "includes/db/departments/update.inc.php";

    let hiddenField;

    for (let i = 0; i < ids.length; i++) {

      hiddenField = document.createElement('input');
      hiddenField.type = 'hidden';
      hiddenField.name = ids[i].getAttribute("name");
      hiddenField.value = ids[i].getAttribute("info");
      form.appendChild(hiddenField);
    }

    for (let i = 0; i < newInfo.length; i++) {

      hiddenField = document.createElement('input');
      hiddenField.type = 'hidden';
      hiddenField.name = newInfo[i].getAttribute("name");
      hiddenField.value = newInfo[i].value.trim();
      form.appendChild(hiddenField);
    }

    hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = "submitted";
    hiddenField.value = "submit";
    form.appendChild(hiddenField);

    document.body.appendChild(form);
    form.submit();
  }
}

function DuplicateRow(element) {
  const row = element.closest("tr");
  const newInfo = row.getElementsByClassName("info-editable");

  // Dynamically create <input>s in a form and submit it

  const form = document.createElement('form');
  form.method = "post";
  form.action = "includes/db/departments/insert.inc.php";

  let hiddenField;

  for (let i = 0; i < newInfo.length; i++) {

    hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = newInfo[i].getAttribute("name");
    hiddenField.value = newInfo[i].getAttribute("info") + " NEW";
    form.appendChild(hiddenField);
  }
  
  hiddenField = document.createElement('input');
  hiddenField.type = 'hidden';
  hiddenField.name = "submitted";
  hiddenField.value = "submit";
  form.appendChild(hiddenField);

  document.body.appendChild(form);
  form.submit();
}

function DeleteRow(element) {

  const row = element.closest("tr");
  const id = row.getElementsByClassName("info-uneditable")[0].getAttribute("info");
  const name = row.getElementsByClassName("info-editable")[0].getAttribute("info");

  if (confirm("Delete " + name + ")?") == true) {

    const form = document.createElement('form');
    form.method = "post";
    form.action = "includes/db/departments/delete.inc.php";

    let hiddenField = document.createElement('input');

    hiddenField.type = 'hidden';
    hiddenField.name = "id";
    hiddenField.value = id;
    form.appendChild(hiddenField);

    hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = "submitted";
    hiddenField.value = "submit";
    form.appendChild(hiddenField);

    document.body.appendChild(form);
    form.submit();

  }
}