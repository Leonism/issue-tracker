function UpdateRow(element) {

  const row = element.closest("tr");
  const admin = row.getElementsByTagName("input")[0];
  const id = row.getAttribute("id");

  // Dynamically create <input>s in a form and submit it

  const form = document.createElement('form');
  form.method = "post";
  form.action = "includes/db/users/adminupdate.inc.php";

  let hiddenField = document.createElement('input');
  hiddenField.type = 'hidden';
  hiddenField.name = "id";
  hiddenField.value = id;
  form.appendChild(hiddenField);

  hiddenField = document.createElement('input');
  hiddenField.type = 'hidden';
  hiddenField.name = "admin";
  if (admin.checked)
    hiddenField.value = "1";
  else
    hiddenField.value = "0";

  form.appendChild(hiddenField);

  hiddenField = document.createElement('input');
  hiddenField.type = 'hidden';
  hiddenField.name = "submitted";
  hiddenField.value = "submit";
  form.appendChild(hiddenField);

  document.body.appendChild(form);
  form.submit();

}

function DeleteRow(element) {

  const row = element.closest("tr");
  const id = row.getAttribute("id");
  const username = row.getAttribute("username");
  const name = row.getAttribute("fullname");

  if (confirm("Delete " + name + " (" + username + ")?") == true) {

    const form = document.createElement('form');
    form.method = "post";
    form.action = "includes/db/users/delete.inc.php";

    let hiddenField = document.createElement('input');

    hiddenField.type = 'hidden';
    hiddenField.name = "id";
    hiddenField.value = id;
    form.appendChild(hiddenField);

    hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = "submitted";
    hiddenField.value = "submit";
    form.appendChild(hiddenField);

    document.body.appendChild(form);
    form.submit();

  }

}
