const rows = document.getElementsByClassName("issue-row");
for (let i = 0; i < rows.length; i++) {
  rows[i].addEventListener("click", () => {
    window.location.href = "notes.php?issue=" + rows[i].getAttribute("issueID");
  });
}