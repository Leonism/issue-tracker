function updateIssue() {

    const id = document.getElementsByName("id")[0];
    const newInfo = document.getElementsByClassName("info-editable");

    const form = document.createElement('form');
    form.method = "post";
    form.action = "includes/db/issues/update.inc.php";

    let hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = "id";
    hiddenField.value = id.getAttribute("info");
    form.appendChild(hiddenField);

    for (let i = 0; i < newInfo.length; i++) {

        hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = newInfo[i].getAttribute("name");
        hiddenField.value = newInfo[i].value;
        form.appendChild(hiddenField);
    }

    hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = "submitted";
    hiddenField.value = "submit";
    form.appendChild(hiddenField);

    document.body.appendChild(form);
    form.submit();
}