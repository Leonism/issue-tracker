<?php

if (isset($_POST['submitted'])) {

    include_once '../dbh.inc.php';

    $title = trim($_POST['title']);
    $body = trim($_POST['body']);
    $creator = trim($_POST['creator']);
    $owner = trim($_POST['owner']);
    $department = trim($_POST['department']);
    $priority = trim($_POST['priority']);
    $created = date("y-m-d H:m:s");

    $sql = "INSERT INTO issues (title, body, creator, owner, department, priority, created) VALUES (?, ?, ?, ?, ?, ?, ?);";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../issues.php?error=stmtfailed");
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "sssssss", $title, $body, $creator, $owner, $department, $priority, $created);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("Location: ../../../issues.php?add=success");
    mysqli_close($conn);
    exit();
} else {
    header("location: ../../../issues.php");
}
