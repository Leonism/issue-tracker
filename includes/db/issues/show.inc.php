<?php

function showIssues()
{
    include 'includes/db/dbh.inc.php';

    $sql = "SELECT issues.id, issues.title, issues.owner, issues.stage, issues.priority, issues.created, users.name AS cname, u.name AS oname, departments.name AS depname FROM (issues LEFT JOIN users ON (issues.creator=users.id) LEFT JOIN users as u ON issues.owner=u.id LEFT JOIN departments ON issues.department=departments.id);";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr issueID=\"" . $row['id'] . "\" class=\"issue-row\" style=\"cursor: pointer;\"><td>" . $row['id'] .
                "</td><td>" . $row['title'] .
                "</td><td>" . $row['cname'] .
                "</td><td>" . $row['oname'] .
                "</td><td>" . $row['stage'] .
                "</td><td>" . $row['depname'] .
                "</td><td>" . $row['priority'] .
                "</td><td>" . $row['created'] .
                "</td></tr>";
        }
    }
    mysqli_close($conn);
}
