<?php
session_start();

if (isset($_POST['submitted'])) {

    include_once '../dbh.inc.php';

    $id = trim($_POST['id']);
    $owner = trim($_POST['owner']);
    $stage = trim($_POST['stage']);
    $department = trim($_POST['department']);
    $priority = trim($_POST['priority']);
    
    $sql = "UPDATE issues SET owner=?, stage=?, department=?, priority=? WHERE id=?;";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../note.php?issue=" + $id + "&error=stmtfailed");
    }

    mysqli_stmt_bind_param($stmt, "sssss", $owner, $stage, $department, $priority, $id);

    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    header("Location: ../../../notes.php?issue=" . $id . "&update=success");
    mysqli_close($conn);
    exit();
} else {
    header("location: ../../../notes.php?issue=" . $id);
}
