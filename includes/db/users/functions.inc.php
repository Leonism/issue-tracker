<?php

function emptyInputRegister($name, $email, $username, $admin, $pwd, $pwdrepeated)
{
    if (empty($name) || empty($email) || empty($username) || (empty($admin) && $admin !== "0") || empty($pwd) || empty($pwdrepeated)) {
        return true;
    }

    return false;
}

function emptyInputLogin($name, $pwd)
{
    if (empty($name) || empty($pwd)) {
        return true;
    }

    return false;
}

function invalidUsername($username)
{
    if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        return true;
    }

    return false;
}

function invalidEmail($email)
{
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    }

    return false;
}

function pwdCheck($pwd, $pwdrepeated)
{
    if ($pwd !== $pwdrepeated) {
        return true;
    }

    return false;
}

/**
 * Check if the username already exists in the database and if it does, return the info of that user.
 * 
 * @param username the username of the user
 * @return false if username doesn't already exist in the database
 * @return row with all the info from the database if the username already exists
 */
function uidExists($conn, $username)
{
    $sql = "SELECT * FROM users WHERE username = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../login.php?error=stmtfailed");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $username);
    mysqli_stmt_execute($stmt);


    $results = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($results)) {
        return $row;
    } else {
        $results = false;
        return $results;
    }

    mysqli_stmt_close($stmt);
}

function createUser($conn, $name, $email, $username, $admin, $department, $pwd)
{
    $sql = "INSERT INTO users (name, email, username, admin, department, pwd) VALUES (?, ?, ?, ?, ?, ?);";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../register.php?error=stmtfailed");
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "ssssss", $name, $email, $username, $admin, $department, $hashedPwd);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("Location: ../../../users.php?add=success");
    mysqli_close($conn);
    exit();
}

function loginUser($conn, $username, $pwd)
{
    $uidExists = uidExists($conn, $username);
    if ($uidExists === false) {
        header("Location: ../../../login.php?error=wrongcreds");
        exit();
    }

    $pwdHashed = $uidExists['pwd'];
    $checkPwd = password_verify($pwd, $pwdHashed);

    if ($checkPwd === false) {
        header("Location: ../../../login.php?error=wrongcreds");
        exit();
    } else if ($checkPwd === true) {

        session_start();
        $_SESSION["userID"] = $uidExists['id'];
        $_SESSION["userName"] = $uidExists['name'];
        $_SESSION["userEMail"] = $uidExists['email'];
        $_SESSION["userUsername"] = $uidExists['username'];
        $_SESSION["userAdmin"] = $uidExists['admin'];
        $_SESSION["userDepartment"] = $uidExists['department'];
        header("Location: ../../../issues.php");
        exit();
    }
}

/**
 * Echoes all the users as options in a drop list (<select>) for html. 
 */
function ShowUsersOptions()
{
    include 'includes/db/dbh.inc.php';

    $sql = "SELECT * FROM users;";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo "<option value=\"" . $row['id'] . "\">" . $row['name'] . "</option>";
        }
    }
    mysqli_close($conn);
}

/**
 * Echoes all the users as options in a drop list (<select>) for html and selects the user 'assignee'. 
 * 
 * @param assignee the id of the user to be automatically selected in the drop down list.
 */
function getAssigneeOptions($assignee)
{
    include 'includes/db/dbh.inc.php';

    $sql = "SELECT * FROM users;";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo "<option value=\"" . $row['id'] . "\"";

            if ($assignee == $row['id'])
                echo " selected=\"selected\"";

            echo ">" . $row['name'] . "</option>";
        }
    }
    mysqli_close($conn);
}
