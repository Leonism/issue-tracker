<?php

if (isset($_POST['submitted'])) {
    include_once '../dbh.inc.php';

    $id = $_POST['id'];
    $admin = $_POST['admin'];

    $sql = "UPDATE users SET admin=? WHERE id=?;";
    
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../users.php?error=stmtfailed");
    }

    mysqli_stmt_bind_param($stmt, "ss", $admin, $id);

    if (mysqli_stmt_execute($stmt) !== false) {
        header("Location: ../../../users.php?update=success");
    }

    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else {
    header("location: ../../../users.php");
}
