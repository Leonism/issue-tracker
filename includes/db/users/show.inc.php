<?php

include_once 'includes/db/dbh.inc.php';

function ShowData()
{

    global $conn;
    $sql = "SELECT users.id, users.name, users.email, users.username, users.admin, users.department, departments.name AS depname FROM users LEFT JOIN departments ON users.department=departments.id;";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            //TODO: we could allow admins see the ID of a user

            echo "<tr id=\"" . $row['id'] . "\" fullname=\"" . $row['name'] . "\" username=\"" . $row['username'] . "\"></td><td info=\"" . $row['name'] . "\" class=\"info-editable\">" . $row['name'] .
                "</td><td info=\"" . $row['username'] . "\" class=\"info-editable\">" . $row['username'] .
                "</td><td info=\"" . $row['email'] . "\" class=\"info-editable\">" . $row['email'] .
                "</td><td info=\"" . $row['department'] . "\" class=\"info-editable\">" . $row['depname'] .
                "<td> <input type=\"checkbox\" class=\"ml-1\" name=\"admin\" value=\"" . $row['admin'] .
                "\"";
            if ($row['admin'] == 1) {
                echo " checked";
            }
            echo "/></td>";
            if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                echo "<td>
                <a href='#' class='btn btn-info btn-circle btn-sm btn-update' onclick='UpdateRow(this)'>
                    <i class='fas fa-floppy-disk'></i>
                </a>
                <a href='#' class='btn btn-danger btn-circle btn-sm' onclick='DeleteRow(this)'>
                    <i class='fas fa-trash'></i>
                </a>
                </td>";
            }
            echo "</tr>";
        }
    }
    mysqli_close($conn);
}
