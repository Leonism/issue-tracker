<?php

if (isset($_POST['submitted'])) {

    include_once '../dbh.inc.php';
    include_once 'functions.inc.php';

    $username = trim($_POST['username']);
    $pwd = trim($_POST['pwd']);

    // If the method doesn't return false, then there is some kind of an error
    if (emptyInputLogin($username, $pwd) !== false) {
        header("location: ../../../login.php?error=infomissing");
        exit();
    }

    loginUser($conn, $username, $pwd);
    mysqli_close($conn);
} else {
    header("location: ../../../login.php");
    exit();
}
