<?php

if (isset($_POST['submitted'])) {

    include_once '../dbh.inc.php';
    include_once 'functions.inc.php';

    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $username = trim($_POST['username']);
    $admin = $_POST['admin'];
    $department = trim($_POST['department']);
    $pwd = trim($_POST['pwd']);
    $pwdrepeated = trim($_POST['pwdrepeated']);

    // If the method doesn't return false, then there is some kind of an error
    if (emptyInputRegister($name, $email, $username, $admin, $pwd, $pwdrepeated) !== false) {
        header("location: ../../../register.php?error=infomissing");
        exit();
    }

    if (invalidUsername($username) !== false) {
        header("location: ../../../register.php?error=invalidusername");
        exit();
    }

    if (invalidEmail($email) !== false) {
        header("location: ../../../register.php?error=invalidEmail");
        exit();
    }

    if (pwdCheck($pwd, $pwdrepeated) !== false) {
        header("location: ../../../register.php?error=pwdnotmatched");
        exit();
    }

    if (uidExists($conn, $username) !== false) {
        header("location: ../../../register.php?error=usernametaken");
        exit();
    }

    createUser($conn, $name, $email, $username, $admin, $department, $pwd);
} else {
    header("location: ../../../register.php");
    exit();
}
