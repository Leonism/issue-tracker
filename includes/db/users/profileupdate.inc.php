<?php
/**
 * Update the profile of the logged in user.
 */
session_start();

if (isset($_POST['submitted']) && isset($_SESSION['userUsername'])) {

    include_once '../dbh.inc.php';
    include_once 'functions.inc.php';

    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $department = trim($_POST['department']);
    $username = $_SESSION['userUsername'];

    $sql = "UPDATE users SET name=?, email=?, department=? WHERE username=?;";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../profile.php?error=stmtfailed");
    }

    mysqli_stmt_bind_param($stmt, "ssss", $name, $email, $department, $_SESSION["userUsername"]);

    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    session_unset();
    session_destroy();
    session_start();

    $uidExists = uidExists($conn, $username);
    $_SESSION["userID"] = $uidExists['id'];
    $_SESSION["userName"] = $uidExists['name'];
    $_SESSION["userEMail"] = $uidExists['email'];
    $_SESSION["userUsername"] = $uidExists['username'];
    $_SESSION["userAdmin"] = $uidExists['admin'];
    $_SESSION["userDepartment"] = $uidExists['department'];

    header("Location: ../../../profile.php?update=success");
    mysqli_close($conn);
    exit();
} else {
    header("location: ../../../login.php");
}
