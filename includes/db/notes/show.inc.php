<?php

include 'includes/db/dbh.inc.php';
include_once 'includes/db/departments/functions.inc.php';
include_once 'includes/db/users/functions.inc.php';

function showIssueNote($issueID)
{

    global $conn;

    /*TODO: Add function to check if such an issue exists
    TODO: Style the main issue note to look different*/

    $sql = "SELECT issues.id, issues.title, issues.body, issues.owner, issues.stage, issues.department, issues.priority, issues.created, users.id AS cid, users.name AS cname, u.id AS oid, u.name AS oname FROM issues LEFT JOIN users ON (issues.creator=users.id) LEFT JOIN users as u on issues.owner=u.id WHERE issues.id='$issueID';";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            if (isset($_SESSION["userName"])) {
                echo "<div class=\"card shadow mb-4\">
                <div class=\"card-header py-3 d-flex justify-content-between\">
                    <h6 class=\"m-0 font-weight-bold text-primary\">" . $row['title'] . "</h6>
                    <a id=\"add-pc-btn\" class=\"btn btn-info btn-circle btn-sm mr-2\">
                        <i class=\"fa-solid fa-plus\"></i>
                    </a>
                </div>
                <div class=\"d-flex justify-content-between card-body pb-0 pt-0 mb-1 mt-1\">
                <div class=\"text-center\">
                    <div>ID:</div>
                    <div name=\"id\" info=\"" . $row['id'] . "\">" . $row['id'] . "</div>
                </div>
                <div class=\"text-center\">
                    <div>By:</div>
                    <div>" . $row['cname'] . "</div>
                </div>
                <div class=\"text-center\">
                <div name=\"owner\" class=\"info-editable\" info=\"";
                if ($row['oname'] == null || $row['oname'] == "")
                    echo "\">Assignee:</div>";
                else
                    echo $row['oid'] . "\">Assignee:</div>";
                echo "<select class=\"form-control w-auto h-auto p-0 info-editable\" name=\"owner\" onchange=\"updateIssue()\">
                        <option value=\"\">None!</option>";
                getAssigneeOptions($row['oid']);
                echo "</select>
                </div>
                <div class=\"text-center\">
                    <div>Stage:</div>
                    <select class=\"form-control w-auto h-auto p-0 info-editable\" name=\"stage\" onchange=\"updateIssue()\">
                    <option value=\"Open\"";
                if ($row['stage'] == "Open")
                    echo " selected=\"selected\"";
                echo ">Open</option>
                    <option value=\"In Progress\"";
                if ($row['stage'] == "In Progress")
                    echo " selected=\"selected\"";
                echo ">In Progress</option>
                    <option value=\"Resolved\"";
                if ($row['stage'] == "Resolved")
                    echo " selected=\"selected\"";
                echo ">Resolved</option>
                <option value=\"Closed\"";
                if ($row['stage'] == "Closed")
                    echo " selected=\"selected\"";
                echo ">Closed</option>
                    </select>
                </div>
                <div class=\"text-center\">
                <div>Department:</div>
                <select class=\"form-control w-auto h-auto p-0 info-editable\" name=\"department\" onchange=\"updateIssue()\">
                    <option value=\"\">No Depatment</option>";
                showIssueDepartment($row['department']);
                echo "</select>
                </div>
                <div class=\"text-center\">
                    <div>Priority:</div>
                    <select class=\"form-control w-auto h-auto p-0 info-editable\" name=\"priority\" onchange=\"updateIssue()\">
                    <option value=\"Low\"";
                if ($row['priority'] == "Low")
                    echo " selected=\"selected\"";
                echo ">Low</option>
                    <option value=\"Normal\"";
                if ($row['priority'] == "Normal")
                    echo " selected=\"selected\"";
                echo ">Normal</option>
                    <option value=\"High\"";
                if ($row['priority'] == "High")
                    echo " selected=\"selected\"";
                echo ">High</option>
                    </select>
                </div>
                <div class=\"text-center\">
                    <div>Created on:</div><div>" . $row['created'] . "</div>
                </div>
                </div>
                <!-- Divider -->
                <hr class=\"sidebar-divider d-none d-md-block mb-0 mt-0\" />
                <div class=\"card-body\">" . $row['body'] .
                    "</div></div>";
            } else {
                echo "<div class=\"card shadow mb-4\">
                <div class=\"card-header py-3 d-flex justify-content-between\">
                    <h6 class=\"m-0 font-weight-bold text-primary\">" . $row['title'] . "</h6>
                    <div class=\"d-flex align-items-center\">
                        Login to submit a note!
                        <a id=\"add-pc-btn\" class=\"ml-2 btn btn-info btn-circle btn-sm mr-2 disabled\">
                            <i class=\"fa-solid fa-plus\"></i>
                        </a>
                    </div>
                </div>
                <div class=\"card-body\">" . $row['body'] .
                    "</div></div>";
            }
        }
    }

    showNotes($issueID);

    mysqli_close($conn);
}

function showNotes($issueID)
{

    global $conn;

    $sql = "SELECT * FROM notes LEFT JOIN users on notes.creator=users.id WHERE issueID='$issueID';";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<div class=\"card shadow mb-4\">
                <div class=\"card-header py-3 d-flex justify-content-between\">
                <h6 class=\"m-0 font-weight-bold text-primary\">" . $row['title'] . "</h6>
                <div class=\"d-flex align-items-center\">
                         " . $row['name'] .
                " - " . $row['created'] .
                "</div>
                </div>
                <div class=\"card-body\">" . $row['body'] .
                "</div></div>";
        }
    }
}
