<?php

if (isset($_POST['submitted'])) {

    include_once '../dbh.inc.php';

    $issueID = trim($_POST['issueID']);
    $title = trim($_POST['title']);
    $body = trim($_POST['body']);
    $creator = trim($_POST['creator']);
    $created = date("y-m-d H:m:s");

    $sql = "INSERT INTO notes (issueID, title, body, creator, created) VALUES (?, ?, ?, ?, ?);";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../../issues.php?error=stmtfailed");
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "sssss", $issueID, $title, $body, $creator, $created);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("Location: ../../../notes.php?issue=" . $issueID);
    mysqli_close($conn);
    exit();
} else {
    header("location: ../../../issues.php");
}
