<?php

function showDepartmentsOptions()
{
    include 'includes/db/dbh.inc.php';

    $sql = "SELECT * FROM departments;";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo "<option value=\"" . $row['id'] . "\"";
            if (isset($_SESSION["userDepartment"]) && $_SESSION["userDepartment"] == $row['id'])
                echo  " selected=\"selected\"";
            echo ">" . $row['name'] . "</option>";
        }
    }
    mysqli_close($conn);
}

function getDepartmentName()
{

    include 'includes/db/dbh.inc.php';

    $sql = "SELECT name FROM departments WHERE id=" . $_SESSION["userDepartment"] . ";";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            return $row['name'];
        }
    }
    mysqli_close($conn);
}

function showIssueDepartment($dep)
{
    include 'includes/db/dbh.inc.php';

    $sql = "SELECT * FROM departments;";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo "<option value=\"" . $row['id'] . "\"";
            if ($dep == $row['id'])
                echo  " selected=\"selected\"";
            echo ">" . $row['name'] . "</option>";
        }
    }
    mysqli_close($conn);
}
