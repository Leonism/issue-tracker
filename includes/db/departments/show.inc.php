<?php

include_once 'includes/db/dbh.inc.php';

function ShowData()
{

    // TODO: Implement deleting departments. CAUTION: users and issues in the deleted department must be addressed

    global $conn;
    $sql = "SELECT * FROM departments;";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);

    if ($numRows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo "<tr><td info=\"" . $row['id'] . "\" name=\"id\" class=\"info-uneditable\">" . $row['id'] .
                "</td><td info=\"" . $row['name'] . "\" name=\"name\" class=\"info-editable\">" . $row['name'] .
                "</label></td>";

            if (isset($_SESSION["userAdmin"]) && $_SESSION["userAdmin"] == "1") {
                echo "<td>
                <a href='#' class='btn btn-info btn-circle btn-sm btn-edit' onclick='EditRow(this)'>
                    <i class='fas fa-pen'></i>
                </a>
                <a href='#' class='btn bg-gray-800 btn-circle btn-sm btn-duplicate' onclick='DuplicateRow(this)'>
                    <i class='fas fa-copy text-white'></i>
                </a>
                <a href='#' class='btn btn-info btn-circle btn-sm btn-update' onclick='UpdateRow(this)' style='display:none'>
                    <i class='fas fa-floppy-disk'></i>
                </a>
                <a href='#' class='btn btn-warning btn-circle btn-sm btn-cancel' onclick='CancelEdit(this)' style='display:none'>
                    <i class='fas fa-close'></i>
                </a>
                <a href='#' class='btn btn-danger btn-circle btn-sm' onclick='DeleteRow(this)'>
                    <i class='fas fa-trash'></i>
                </a>
            </td>";
            }
            echo "</tr>";
        }
    }
    mysqli_close($conn);
}
